import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  id: any;
  name: string;
  products: any;

  constructor(private dataService: DataService,
    private route: ActivatedRoute,) { }

ngOnInit(): void {
  this.route.params.subscribe((params) => {
    this.id = params['id'];
  });

}
ngAfterViewInit():void{
  this.dataService.viewProduct(this.id).subscribe((element: any[]) => {
    this.products = element;
  })
}
closeModal(){
  this.dataService.$modal.emit(false)
}

}