import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
  products = [];
  modalSwitch:boolean
  constructor(private dataService: DataService,private router: Router) { }

  ngOnInit(): void {
    this.dataService.$modal.subscribe((valor)=> (this.modalSwitch = valor))
    this.dataService.sendGetRequest().subscribe((data: any[]) => {
      console.log(data);
      this.products = data;
    });

   
  }

  openModal(){
    this.modalSwitch = true
  }

  verProducto(id: number){
    this.router.navigate(['product', id])
  }

  // verProductoEnModal(){
  //   this.openModal,
  //   this.verProducto

  // }


}
