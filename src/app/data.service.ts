import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  private REST_API_SERVER = 'http://localhost:3000/products';
  constructor(private httpClient: HttpClient) { }

  public sendGetRequest(): Observable<any> {
    return this.httpClient.get(this.REST_API_SERVER);
  }

  //cambiar estado del modal
$modal = new EventEmitter<any>()

//obtenr el producto por id para mostrar en el modal
public viewProduct(id: any): Observable<any>{
  return this.httpClient.get(this.REST_API_SERVER + "?id=" + id);
}

}
